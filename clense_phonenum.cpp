#include <iostream>
#include <string.h>
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;

int main(void){
    string content;
    fstream fs;
    fs.open("users.csv", ios::in);
    int count = 0;
    while(!fs.eof()){
        count++;
        getline(fs,content,',');
        if(count % 3 == 0){
            for(int i = 0; i < content.size(); i++){
                if(content[i] >= '0' && content[i] <= '9') continue;
                else content[i] = '-';
            }
            cout << content << "\n";
        }
    }
    fs.close();
}
