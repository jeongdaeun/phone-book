# phone-book

## get_user_info.cpp

1. 사용자 이름(Firstname Lastname), 이메일(id@domain.com), 전화번호(010-1234-1234)를 순서대로 입력하시오.
2. csv파일에 사용자의 정보가 입력된 것을 확인할 수 있습니다.

## get_domain.cpp

1. 사용자 아이디와 도메인 명을 분리합니다.
2. 이에 대해 print합니다.

## clense_phonenum.cpp

1. 전화번호 형식을 지키지 않았을 경우 특수문자를 '-'로 치환합니다.
2. 이에 대해 print합니다.