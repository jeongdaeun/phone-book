#include <iostream>
#include <string.h>
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;

vector<string> split(string str){
    istringstream ss(str);
    string stringBuffer;
    vector<string> x;
    while (getline(ss, stringBuffer, '@')){
        x.push_back(stringBuffer);
    }
    return x;
}

int main(void){
    string content;
    fstream fs;
    fs.open("users.csv", ios::in);
    int count = 0;
    while(!fs.eof()){
        count++;
        getline(fs,content,',');
        if(count % 3 == 2){
            vector<string> emailVector = split(content);
            for(int i = 0; i < emailVector.size(); i++){
                cout << emailVector[i] << "\n";
            }
        }
    }
    fs.close();
}
